# Edit Permission Inheritance Cloud app
Allow edit permission to inherit to all child pages if needed

## License
Please refer to our [Source code license agreement](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/15826959/Source+code+license+agreement)

## Manual
Please refer [this page](https://purde-software.atlassian.net/wiki/x/BIC9eg).

## Branches
The sources contain two branches. As the "dev" branch is work in progress you should only use the sources of the "master" branch.

## Change log

### 1.8.4

Infrastructure:

* Upgrade to Spring Boot 3.4.3
* Upgrade to ACSB 5.1.7

### 1.8.3

Infrastructure:

* Upgrade to Spring Boot 3.4.1
* Upgrade to ACSB 5.1.5 

### 1.8.2

Infrastructure:

* Upgrade to Spring Boot 3.2.11
* Upgrade to ACSB 5.1.4 

### 1.8.1

Improvements:

* Explicitly trigger garbage collection while collecting a huge number of descendants

Infrastructure:

* Upgrade to Spring Boot 3.2.8

### 1.8.0

New features:

* Add deviation report with easy possibility to remove additional permissions

### 1.7.10

Bug fix:

* Fix failing delete operations (415) unknown cause

### 1.7.9

Infrastructure:

* Upgrade to Spring Boot 3.2.5

### 1.7.8

Infrastructure:

* Upgrade to Spring Boot 3.2.3
* Upgrade to ACSB 4.1.1
* Upgrade postgresql

### 1.7.7

Bug fix:

* Fix (assumed) massive memory leak

### 1.7.6

Infrastructure:

* Upgrade to Spring Boot 3.1.7
* Switch to REST API v2

### 1.7.5

Infrastructure:

* Upgrade to ACSB 4.0.7
* Upgrade to Sprint Boot 3.1.6

### 1.7.4

Infrastructure:

* Upgrade to Spring Boot 3.1.5
* Upgrade to ACSB 4.0.5
* Upgrade to JAVA 17
* Upgrade to json 20231013

### 1.7.3

Improvements:

* Change color of icons for better visibility in Confluence's Dark theme

Infrastructure:

* Upgrade to Spring Boot 2.7.16
* Upgrade to ACSB to 3.0.7

### 1.7.2

Bug fix:

* Properly detect no-theme

### 1.7.1

Infrastructure:

* Upgrade to Spring Boot 2.7.14
* Override Thymeleaf version of fix a vulnerability

### 1.7.0

Improvements:

* Support Confluence's Dark theme

Infrastructure:

* Upgrade to Spring Boot 2.7.13
* Upgrade to ACSB to 3.0.4

### 1.6.6

Infrastructure:

* Upgrade to Spring Boot 2.7.11
* Upgrade to ACSB to 3.0.3
* Upgrade json to 20230227

### 1.6.5

Infrastructure:

* Upgrade to Spring Boot 2.7.10
* Remove googelapis
* Base test system on Java 17
* Use TaskExecutor for standard async calls

### 1.6.4

Infrastructure:

* Upgrade to Spring Boot 2.7.8
* Upgrade ACSB to 3.0.2

### 1.6.3

Improvements:

* Check license status before executing the inheritance

Infrastructure:

* Upgrade to Spring Boot 2.7.7

### 1.6.2

Infrastructure:

* Upgrade to Spring Boot 2.7.6
* Upgrade postgresql

### 1.6.1

Infrastructure:

* Upgrade to Spring Boot 2.7.4

### 1.6.0

New features:

* Add a migration UI

### 1.5.5

Infrastructure:

* Upgrade postgresql

### 1.5.4

Infrastructure:

* Upgrade to Spring Boot 2.7.2
* Change autowired instances to private

### 1.5.3

Infrastructure:

* Upgrade to Spring Boot 2.7.1
* Upgrade to Atlassian Connect Spring Boot 2.3.6
* Add CSP header
* Change Referrer Policy Header

### 1.5.2

Infrastructure:

* Upgrade to Spring Boot 2.6.8

### 1.5.1

Infrastructure:

* Update to Atlassian Connect Spring Boot 2.3.4

### 1.5.0

Improvements:

* Show active edit permission inheritances with a different icon color

Infrastructure:

* Remove bundled AUI

### 1.4.0

Improvements:

* Execute actions following webhooks coming from apps as our app

### 1.3.6

Infrastructure:

* Update to Atlassian Connect Spring Boot 2.3.3
* Update to Spring Boot 2.6.6

### 1.3.5

Infrastructure:

* Update to Atlassian Connect Spring Boot 2.3.1
* Update to Spring Boot 2.6.4
* Update other dependencies

### 1.3.4

Infrastructure:

* Update to Spring Boot 2.5.10

### 1.3.3

Bug fixes:

* Fix omission detection in case of moved pages

### 1.3.2

Infrastructure:

* Update to Spring Boot 2.5.9
* Update to Atlassian Connect Spring Boot 2.2.6
* Upgrade postgresql driver to avoid CVE-2022-21724

### 1.3.1

Infrastructure:

* Update to Spring Boot 2.5.8

### 1.3.0

Improvements:

* Add an option to delay the application of the inheritance
* Show number of sub-inherits in the consistency check
* Show of number of omitted descendants in the consistency check

Infrastructure:

* Update to Spring Boot 2.5.6

### 1.2.2

Infrastructure:

* Update to Atlassian Connect Spring Boot 2.2.3

### 1.2.1

Bug fixes:

* Skip webhook events triggered by apps

### 1.2.0

Infrastructure:

* Switch to group ids rather than group names
* Atlassian Connect installation lifecycle security improvements

### 1.1.7

Bug fixes:

* Send Strict-Transport-Security as required by Atlassian

### 1.1.6

Bug fixes:

* Context QSH related fix as required by Atlassian

### 1.1.4 and 1.1.5

Bug fixes:

* Properly handle pages added with an ID above MAX_INT

### 1.1.3

Improvements:

* Do not add an edit permission in case the user or group does not have a direct view permission

### 1.1.1 and 1.1.2

Infrastructure:

* Solve Coverity defects
* Add additional logs

### 1.1.0

Improvements:

* Implement better consistency check
* Avoid inheritance in case user has no personal edit permission
* Various bug fixes before release

### 1.0.0

Initial release for Marketplace Approval


