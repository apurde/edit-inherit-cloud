package de.edrup.confluence.plugins.editinheritcloud.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHost;

import de.edrup.confluence.plugins.editinheritcloud.util.EditInheritHelper;
import de.edrup.confluence.plugins.editinheritcloud.util.MiniJSONDotAccess;

@Service
public class EditInheritCore {
	
	@Autowired
	private EditInheritHelper editInheritHelper;
	
	@Autowired
	TaskExecutor executor;
	
	private static final Logger log = LoggerFactory.getLogger(EditInheritCore.class);
	private static final int DESC_LIMIT = 200;
	private static final int POOL_SIZE = 1;
	private static final MiniJSONDotAccess da = new MiniJSONDotAccess();
	private ConcurrentHashMap<String, Long> runningInheritances = new ConcurrentHashMap<String, Long>();
	private List<String> repeatRequested = Collections.synchronizedList(new ArrayList<String>());
	private List<String> pagesToProcess = Collections.synchronizedList(new ArrayList<String>());
	private HashMap<String, CompletableFuture<JSONObject>> futureRequests = new HashMap<String, CompletableFuture<JSONObject>>();
	
	
	public void inheritEditPermissions(AtlassianHost host, String useAccountId, String parentPageId, String startPageId, boolean fixMode, boolean stampOnly) {
		CompletableFuture.supplyAsync(() -> filterInheritRequest(host, useAccountId, parentPageId, startPageId, fixMode, stampOnly), executor);
	}
	
	
	public boolean hasEditPermissionInheritance(AtlassianHost host, String useAccountId, String pageId) {
		return da.getBoolean(getInheritStatusForPage(host, useAccountId, pageId), "value.enabled", false);
	}
	
	
	public boolean shouldSkipEvent(String pageId) {
		return pagesToProcess.contains(pageId);
	}
	
	
	public String getClosestEditPermissionInheritancePageId(AtlassianHost host, String useAccountId, String pageId) {
		
		// get all ancestor ids
		JSONArray ancestors = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/api/v2/pages/%s/ancestors?limit=200", host.getBaseUrl(), pageId)).getJSONArray("results");
		List<String> ancestorsIds = new ArrayList<String>();
		for(int n = 0; n < ancestors.length(); n++) ancestorsIds.add(ancestors.getJSONObject(n).getString("id"));
		
		log.debug("Found {} ancestors", ancestorsIds.size());
		
		// get the edit-inherit-options on all ancestors
		HashMap<String, JSONObject> inheritsOnAncestors = editInheritHelper.getManyJSONObject(host, useAccountId, String.format("%s/api/v2/pages/$/properties?key=edit-inherit-options", host.getBaseUrl()), ancestorsIds);
		
		log.debug("Collected all inherits an ancestors");
		
		// find the closest ancestors with an active edit permission inheritance
		String inheritPageId = null;
		for(int n = 0; n < ancestors.length(); n++) {
			String ancestorId = ancestors.getJSONObject(n).getString("id");
			JSONObject editInheritOptions = inheritsOnAncestors.get(ancestorId).getJSONArray("results").length() > 0 ? inheritsOnAncestors.get(ancestorId).getJSONArray("results").getJSONObject(0) : new JSONObject();
			if(da.getBoolean(editInheritOptions, "value.enabled", false)) {
				inheritPageId = ancestorId;
			}
		}
		
		return inheritPageId;
	}
	
	
	public String findSuitableAccountId(AtlassianHost host, String useAccountId, String parentPageId, String startPageId) {
		
		String logKey = host.getBaseUrl() + ";" + parentPageId + ";" + startPageId;
		
		try {
			List<String> candidates = new ArrayList<String>();
			
			JSONObject restrictions = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/content/%s/restriction/byOperation/update", host.getBaseUrl(), parentPageId));
			JSONObject eiLast = editInheritHelper.getContentProperty(host, useAccountId, parentPageId, "edit-inherit-last");
			
			String usedAccountId = da.getString(eiLast, "value.usedAccountId", "");
			if(!usedAccountId.isEmpty()) {
				candidates.add(usedAccountId);
			}
			candidates.addAll(StreamSupport.stream(da.getJSONArray(restrictions, "restrictions.user.results").spliterator(), false)
				.map(x -> ((JSONObject) x).getString("accountId")).collect(Collectors.toList()));
			
			candidates.add(useAccountId);
			
			for(String candidate : candidates) {
				int statusCode = editInheritHelper.putNoPayload(host, candidate, String.format("%s/rest/api/content/%s/restriction/byOperation/update/user?accountId=%s", host.getBaseUrl(), startPageId, "1"));
				if(statusCode != 403) {
					return candidate;
				}
			}
			
			log.warn("{}: could not find a suitable accountId", logKey);
			return null;
		}
		catch(Exception e) {
			log.error("{}: error finding suitable accountId: {}", logKey, e.toString());
			return null;
		}
	}
	
	
	public JSONObject getEditPermissionInheritanceStatus(AtlassianHost host, String useAccountId, String pageId) {
		
		JSONObject status = new JSONObject();
		
		try {
			JSONObject inheritStatus = getInheritStatusForPage(host, useAccountId, pageId);
			boolean enabled = da.getBoolean(inheritStatus, "value.enabled", false);
			
			JSONObject restrictions = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/content/%s/restriction/byOperation/update", host.getBaseUrl(), pageId));
			
			List<String> pageAccountIds = StreamSupport.stream(da.getJSONArray(restrictions, "restrictions.user.results").spliterator(), false)
				.map(x -> ((JSONObject) x).getString("accountId")).collect(Collectors.toList());
			List<String> pageGroupIds = StreamSupport.stream(da.getJSONArray(restrictions, "restrictions.group.results").spliterator(), false)
				.map(x -> ((JSONObject) x).getString("id")).collect(Collectors.toList());
			
			status.put("canInherit", (pageAccountIds.isEmpty() && pageGroupIds.isEmpty()) || pageAccountIds.contains(useAccountId));
			status.put("enabled", enabled);
			status.put("options", da.getString(inheritStatus, "value.options", ""));
			status.put("running", runningInheritances.containsKey(host.getBaseUrl().concat(pageId)));
			if(!enabled) {
				status.put("closestPageId", getClosestEditPermissionInheritancePageId(host, useAccountId, pageId));
			}
		}
		catch(Exception e) {
			log.error("{}: error getting inheritance status: {}", host.getBaseUrl() + ";" + pageId, e.toString());
			log.trace("Trace: ", e);
		}
		
		return status;
	}
	
	
	public JSONObject getDeviations(AtlassianHost host, String useAccountId, String pageId, String requestId) {
		JSONObject result = new JSONObject();
		try {
			if(futureRequests.containsKey(requestId) && futureRequests.get(requestId).isDone()) {
				result.put("finished", true);
				JSONObject clone = new JSONObject(futureRequests.get(requestId).get().toString());
				result.put("nonCompliantDescendants", clone.getJSONArray("nonCompliantDescendants"));
				result.put("subInherits", clone.getJSONArray("subInherits"));
				result.put("omittedDescendants", clone.getJSONArray("omittedDescendants"));
				futureRequests.remove(requestId);
			}
			else if(futureRequests.containsKey(requestId) && !futureRequests.get(requestId).isDone()) {
				result.put("finished", false);
			}
			else {
				result.put("finished", false);
				futureRequests.put(requestId, CompletableFuture.supplyAsync(() -> findDeviations(host, useAccountId, pageId, requestId), executor));
			}
		}
		catch(Exception e) {
			log.error("{}: could not check for non compliant inhertiances: {}", host.getBaseUrl() + ";" + pageId, e.toString());
			log.trace("Trace: ", e);			
		}
		return result;
	}
	
	
	public JSONObject getDescendantsForReport(AtlassianHost host, String useAccountId, String pageId, String requestId) {
		JSONObject result = new JSONObject();
		try {
			if(futureRequests.containsKey(requestId) && futureRequests.get(requestId).isDone()) {
				result.put("finished", true);
				JSONObject clone = new JSONObject(futureRequests.get(requestId).get().toString());
				result.put("descendants", clone.getJSONArray("descendants"));
				futureRequests.remove(requestId);
			}
			else if(futureRequests.containsKey(requestId) && !futureRequests.get(requestId).isDone()) {
				result.put("finished", false);
			}
			else {
				result.put("finished", false);
				futureRequests.put(requestId, CompletableFuture.supplyAsync(() -> getRelevantDescendants(host, useAccountId, pageId, true, false), executor));
			}
		}
		catch(Exception e) {
			log.error("{}: could not get descendants: {}", host.getBaseUrl() + ";" + pageId, e.toString());
			log.trace("Trace: ", e);			
		}
		return result;
	};
	
	
	public JSONObject setInheritance(AtlassianHost host, String useAccountId, JSONObject setData) {
		JSONObject result = new JSONObject();
		
		try {
			String pageId = setData.getString("pageId");
			String accountId = findSuitableAccountId(host, useAccountId, pageId, pageId);
			
			if(accountId != null) {
				JSONObject inheritStatus = getInheritStatusForPage(host, useAccountId, pageId);
				JSONObject inheritOptions = new JSONObject().put("enabled", true).put("options", setData.getString("options"));
				editInheritHelper.setContentProperty(host, accountId, pageId, "edit-inherit-options", inheritOptions, inheritStatus);
				result.put("success", true);
			}
			else {
				result.put("success", false).put("error", "No suitable accountId found");
			}
		}
		catch(Exception e) {
			result.put("success", false).put("error", e.toString());
		}
		
		return result;
	}
	
	
	private JSONObject getInheritStatusForPage(AtlassianHost host, String useAccountId, String pageId) {
		return editInheritHelper.getContentProperty(host, useAccountId, pageId, "edit-inherit-options");
	}
	
	
	private boolean filterInheritRequest(AtlassianHost host, String useAccountId, String parentPageId, String startPageId, boolean fixMode, boolean stampOnly) {
		
		String logKey = host.getBaseUrl() + ";" + parentPageId + ";" + startPageId;
		
		try {
			Thread.sleep(ThreadLocalRandom.current().nextInt(10, 400 + 1));
		
			String key = host.getBaseUrl().concat(startPageId);
			Long now = new Date().getTime();
				
			if(runningInheritances.containsKey(key) && (now - runningInheritances.get(key)) < 2000) {
				log.info("{}: edit permission inheritance skipped due to 2 second rule", logKey);
				return false;
			}
			if(runningInheritances.containsKey(key) && (now - runningInheritances.get(key)) > 2000) {
				repeatRequested.add(key);
				runningInheritances.replace(key, now);
				log.info("{}: edit permission inheritance repeat requested", logKey);
				return false;
			}
			
			runningInheritances.put(key, now);
		
			doinheritEditPermissions(host, useAccountId, parentPageId, startPageId, fixMode, stampOnly);
			while(repeatRequested.contains(key)) {
				repeatRequested.remove(key);
				doinheritEditPermissions(host, useAccountId, parentPageId, startPageId, fixMode, stampOnly);
			}
			
			runningInheritances.remove(key);		
			
			return true;
		}
		catch(Exception e) {
			log.error("{}: error in filter requests: {}", logKey, e.toString());
			log.trace("Trace: ", e);
			return false;
		}
	}
	
	
	private boolean doinheritEditPermissions(AtlassianHost host, String useAccountId, String parentPageId, String startPageId, boolean fixMode, boolean stampOnly) {
		
		String logKey = host.getBaseUrl() + ";" + parentPageId + ";" + startPageId;
		ExecutorService pool = null;
		
		log.info("{}: edit permission inheritance started", logKey);
				
		try {		
			JSONObject license = editInheritHelper.getJSONObject(host, null, String.format("%s/rest/atlassian-connect/1/addons/de.edrup.confluence.plugins.edit-inherit",  host.getBaseUrl()));
			if(!da.getBoolean(license, "license.active", true) && !host.getBaseUrl().contains("edrup-dev")) {
				log.info("{}: skipping further actions as app is not licensed", logKey);
				return false;
			}
			
			ConcurrentHashMap<String, String> groupsOfUser = new ConcurrentHashMap<String, String>();
			
			JSONObject restrictions = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/content/%s/restriction/byOperation/update", host.getBaseUrl(), parentPageId));
			JSONObject eiOptions = editInheritHelper.getContentProperty(host, useAccountId, parentPageId, "edit-inherit-options");
			JSONObject eiLast = editInheritHelper.getContentProperty(host, useAccountId, parentPageId, "edit-inherit-last");
			JSONObject eiStamp = editInheritHelper.getContentProperty(host, useAccountId, parentPageId, "edit-inherit-stamp");			
			
			boolean enabled = da.getBoolean(eiOptions, "value.enabled", false);
			String inheritOptions = da.getString(eiOptions, "value.options", "");
			String stamp = (parentPageId.equals(startPageId) && !fixMode) ? UUID.randomUUID().toString() : da.getString(eiStamp, "value", "");
			boolean topInheritance = parentPageId.equals(startPageId);
			log.debug("{}: inheritance is will run enabled {} with options {} and fix mode {}", logKey, enabled, inheritOptions, fixMode);
			log.debug("{}: using stamp {}", logKey, stamp);
			
			if(enabled && topInheritance && !fixMode) {
				editInheritHelper.setContentProperty(host, useAccountId, parentPageId, "edit-inherit-stamp", stamp, eiStamp);
			}
			
			List<String> accountIds = StreamSupport.stream(da.getJSONArray(restrictions, "restrictions.user.results").spliterator(), false)
				.map(x -> ((JSONObject) x).getString("accountId")).collect(Collectors.toList());
			List<String> groupIds = StreamSupport.stream(da.getJSONArray(restrictions, "restrictions.group.results").spliterator(), false)
				.map(x -> ((JSONObject) x).getString("id")).collect(Collectors.toList());
			log.debug("{}: Found {} user permissions and {} group permissions on the parent", logKey, accountIds.size(), groupIds.size());
			
			JSONObject lastParentPermissions = new JSONObject(da.getString(eiLast, "value", "{}"));
			log.debug("{}: last parent permission is {}", logKey, lastParentPermissions.toString());
			
			if(inheritOptions.contains("delay")) {
				log.debug("Delaying the start...");
				try {
					Thread.sleep(120000L);
				}
				catch(Exception e) {
					log.error(e.toString());
				}
			}

			List<String> lastParentAccountIds = splitNotEmpty(da.getString(lastParentPermissions, "user", ""));
			List<String> lastParentGroupIds = splitNotEmpty(da.getString(lastParentPermissions, "groupIds", ""));
			
			if(lastParentGroupIds.isEmpty()) {
				List<String> lastParentGroupNames = splitNotEmpty(da.getString(lastParentPermissions, "group", ""));
				for(String lastParentGroupName : lastParentGroupNames) {
					try {
						JSONObject group = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/group/by-name?name=%s", host.getBaseUrl(), lastParentGroupName));
						String groupId = da.getString(group, "id", "");
						if(!groupId.isEmpty()) {
							lastParentGroupIds.add(groupId);
							log.debug("{}: mapped group name {} to group id {}", logKey, lastParentGroupName, groupId);
						}
					}
					catch(Exception e) {
						log.error("{}: error finding group id for {}: {}", logKey, lastParentGroupName, e.toString());
					}
				}
			}
												
			List<CompletableFuture<Boolean>> futures = new ArrayList<CompletableFuture<Boolean>>();
			List<String> pageIds = new ArrayList<String>();
			if(enabled) {
				JSONObject descendantsResult = getRelevantDescendants(host, useAccountId, startPageId, !parentPageId.equals(startPageId), !parentPageId.equals(startPageId));
				JSONArray descendants = descendantsResult.getJSONArray("descendants");
				pageIds.addAll(StreamSupport.stream(descendants.spliterator(), false).map(x -> ((JSONObject) x).getString("id")).collect(Collectors.toList()));
				pagesToProcess.addAll(pageIds);
				log.debug("{}: found {} relevant descendants", logKey, descendants.length());
				pool = Executors.newFixedThreadPool(POOL_SIZE);
				for(int n = 0; n < descendants.length(); n++) {
					JSONObject page = descendants.getJSONObject(n);
					if(!fixMode || !da.getString(page, "metadata.properties.edit-inherit-stamp.value", "unknown").equals(stamp)) {
						futures.add(CompletableFuture.supplyAsync(() -> adoptEditPermissionsOnPage(host, useAccountId, page, inheritOptions, lastParentAccountIds, lastParentGroupIds, accountIds, groupIds, stamp, topInheritance && !fixMode, groupsOfUser, stampOnly), pool));
					}
				}
			}
			CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
			combinedFuture.get();
						
			if(enabled && topInheritance && !fixMode) {
				JSONObject updatedParentPermissions = new JSONObject();
				updatedParentPermissions.put("user", String.join(",", accountIds));
				updatedParentPermissions.put("groupIds", String.join(",", groupIds));
				updatedParentPermissions.put("usedAccountId", useAccountId);
				editInheritHelper.setContentProperty(host, useAccountId, parentPageId, "edit-inherit-last", updatedParentPermissions.toString(), eiLast);
			}
			
			pagesToProcess.removeAll(pageIds);
			log.info("{}: edit permission inheritance completed", logKey);
	
			return true;
		}
		catch(Exception e) {
			log.error("{}: error inheriting permissions: {}", logKey, e.toString());
			log.trace("Trace: ", e);
			return false;
		}
		finally {
			if(pool != null) {
				pool.shutdown(); // Shut down the thread pool
				try {
					if(!pool.awaitTermination(60, TimeUnit.SECONDS)) {
						pool.shutdownNow(); // Cancel currently executing tasks
					} 
				} catch (InterruptedException ie) {
					pool.shutdownNow();
					Thread.currentThread().interrupt(); // Preserve interrupt status
				}
			}
		}
	}
	
	
	private Boolean adoptEditPermissionsOnPage(AtlassianHost host, String useAccountId, JSONObject page, String inheritOptions,	List<String> lastParentAccountIds, List<String> lastParentGroupIds,
		List<String> parentAccountIds, List<String> parentGroupIds, String stamp, boolean allowDelta, ConcurrentHashMap<String, String> groupsOfUser, boolean stampOnly) {
		
		String userRestUrl = "%s/rest/api/content/%s/restriction/byOperation/update/user?accountId=%s";
		String groupRestUrl = "%s/rest/api/content/%s/restriction/byOperation/update/byGroupId/%s";
		
		try {
			String pageId = page.getString("id");
			boolean deltaOnly = inheritOptions.contains("delta") && allowDelta;
			boolean success = true;
			boolean hasOmissions = false;
			
			if(!stampOnly) {
			
				List<String> pageAccountIds = StreamSupport.stream(da.getJSONArray(page, "restrictions.update.restrictions.user.results").spliterator(), false)
					.map(x -> ((JSONObject) x).getString("accountId")).collect(Collectors.toList());
				List<String> pageGroupIds = StreamSupport.stream(da.getJSONArray(page, "restrictions.update.restrictions.group.results").spliterator(), false)
					.map(x -> ((JSONObject) x).getString("id")).collect(Collectors.toList());
				List<String> readAccountIds = StreamSupport.stream(da.getJSONArray(page, "restrictions.read.restrictions.user.results").spliterator(), false)
						.map(x -> ((JSONObject) x).getString("accountId")).collect(Collectors.toList());
				List<String> readGroupIds = StreamSupport.stream(da.getJSONArray(page, "restrictions.read.restrictions.group.results").spliterator(), false)
					.map(x -> ((JSONObject) x).getString("id")).collect(Collectors.toList());
	
				
				if(parentAccountIds.isEmpty() && parentGroupIds.isEmpty()) {
					success &= editInheritHelper.delete(host, useAccountId, String.format("%s/rest/api/content/%s/restriction", host.getBaseUrl(), pageId));
				}
				else {
					if(parentAccountIds.indexOf(useAccountId) > 0) {
						Collections.swap(parentAccountIds, parentAccountIds.indexOf(useAccountId), 0);
					}
					for(String parentAccountId : parentAccountIds) {
						if((!lastParentAccountIds.contains(parentAccountId) || !deltaOnly) && !pageAccountIds.contains(parentAccountId)) {
							if(hasUserDirectReadAccess(host, useAccountId, groupsOfUser, parentAccountId, readAccountIds, readGroupIds)) {
								success &= (editInheritHelper.putNoPayload(host, useAccountId, String.format(userRestUrl, host.getBaseUrl(), pageId, parentAccountId)) == 200);
							}
							else {
								hasOmissions = true;
								log.info("Skipping user {} due to missing view permission on {} at {}", parentAccountId, pageId, host.getBaseUrl());
							}
						}
					}
					for(String parentGroupId : parentGroupIds) {
						if((!lastParentGroupIds.contains(parentGroupId) || !deltaOnly) && !pageGroupIds.contains(parentGroupId)) {
							if(hasGroupDirectReadAccess(parentGroupId, readAccountIds, readGroupIds)) {
								success &= (editInheritHelper.putNoPayload(host, useAccountId, String.format(groupRestUrl, host.getBaseUrl(), pageId, parentGroupId)) == 200);
							}
							else {
								hasOmissions = true;
								log.info("Skipping group {} due to missing view permission on {} at {}", parentGroupId, pageId, host.getBaseUrl());
							}
						}
					}
					
					for(String lastParentAccountId : lastParentAccountIds) {
						if(!parentAccountIds.contains(lastParentAccountId) && pageAccountIds.contains(lastParentAccountId)) {
							success &= editInheritHelper.delete(host, useAccountId, String.format(userRestUrl, host.getBaseUrl(), pageId, lastParentAccountId));
							pageAccountIds.remove(lastParentAccountId);
						}
					}
					for(String lastParentGroupId : lastParentGroupIds) {
						if(!parentGroupIds.contains(lastParentGroupId) && pageGroupIds.contains(lastParentGroupId)) {
							success &= editInheritHelper.delete(host, useAccountId, String.format(groupRestUrl, host.getBaseUrl(), pageId, lastParentGroupId));
							pageGroupIds.remove(lastParentGroupId);
						}
					}
					
					String creatorAccountId = da.getString(page, "history.createdBy.accountId", da.getString(page, "authorId", "unknown"));
					
					if(inheritOptions.contains("hard")) {
						for(String pageAccountId : pageAccountIds) {
							if(!parentAccountIds.contains(pageAccountId) && !(inheritOptions.contains("creator") && pageAccountId.equals(creatorAccountId))) {
								success &= editInheritHelper.delete(host, useAccountId, String.format(userRestUrl, host.getBaseUrl(), pageId, pageAccountId));
							}
						}
						for(String pageGroupId : pageGroupIds) {
							if(!parentGroupIds.contains(pageGroupId)) {
								success &= editInheritHelper.delete(host, useAccountId, String.format(groupRestUrl, host.getBaseUrl(), pageId, pageGroupId));
							}
						}
					}
					
					if(inheritOptions.contains("creator") && !pageAccountIds.contains(creatorAccountId)) {
						if(hasUserDirectReadAccess(host, useAccountId, groupsOfUser, creatorAccountId, readAccountIds, readGroupIds)) {
							success &= (editInheritHelper.putNoPayload(host, useAccountId, String.format(userRestUrl, host.getBaseUrl(), pageId, creatorAccountId)) == 200);
						}
						else {
							hasOmissions = true;
							log.info("Skipping user {} due to missing view permission on {} at {}", creatorAccountId, pageId, host.getBaseUrl());
						}
					}
				}
			}
			
			if(success) {
				editInheritHelper.setContentProperty(host, useAccountId, pageId, "edit-inherit-stamp", stamp, da.getJSONObject(page, "metadata.properties.edit-inherit-stamp"));
				editInheritHelper.setContentProperty(host, useAccountId, pageId, "edit-inherit-omission", Boolean.toString(hasOmissions), da.getJSONObject(page, "metadata.properties.edit-inherit-omission"));
			}
			
			return success;
		}
		catch(Exception e) {
			log.error("Error inheriting permission for {} on page {} with options {}: {}", host.getBaseUrl(), da.getString(page, "id", "0"), inheritOptions, e.toString());
			log.trace("Trace: ", e);
		}
		return false;
	}
	
	
	private boolean hasUserDirectReadAccess(AtlassianHost host, String useAccountId, ConcurrentHashMap<String, String> groupsOfUser, String accountId, List<String> readAccountIds, List<String> readGroupIds) {
		if(readAccountIds.isEmpty() && readGroupIds.isEmpty()) {
			return true;
		}
		if(readAccountIds.contains(accountId)) {
			return true;
		}
		if(!Collections.disjoint(getGroupsOfUser(host, useAccountId, groupsOfUser, accountId), readGroupIds)) {
			return true;
		}
		return false;
	}
	
	
	private boolean hasGroupDirectReadAccess(String groupId, List<String> readAccountIds, List<String> readGroupIds) {
		if(readAccountIds.isEmpty() && readGroupIds.isEmpty()) {
			return true;
		}
		else {
			return readGroupIds.contains(groupId);
		}
	}
	
	
	private List<String> getGroupsOfUser(AtlassianHost host, String useAccountId, ConcurrentHashMap<String, String> groupsOfUser, String accountId) {
		if(groupsOfUser.contains(accountId)) {
			return Arrays.asList(groupsOfUser.get(accountId).split(","));
		}
		JSONObject result = editInheritHelper.getJSONObject(host, useAccountId, "/rest/api/user/memberof?accountId=" + accountId);
		List<String> groupIds = StreamSupport.stream(da.getJSONArray(result, "results").spliterator(), false)
			.map(x -> ((JSONObject) x).getString("id")).collect(Collectors.toList());
		groupsOfUser.put(accountId, String.join(",", groupIds));
		return groupIds;
	}
	
	
	private JSONObject getRelevantDescendants(AtlassianHost host, String useAccountId, String pageId, boolean includeRoot, boolean handleRootAsSubInherit) {
		JSONObject result = new JSONObject();
		JSONArray allDescendants = getAllDescendants(host, useAccountId, pageId, includeRoot);
		log.debug("Collected {} descendants including root {} for {} and page {}", allDescendants.length(), includeRoot, host.getBaseUrl(), pageId);
		
		List<String> subInheritIds = StreamSupport.stream(allDescendants.spliterator(), false)
			.filter(x -> da.getBoolean((JSONObject) x, "metadata.properties.edit-inherit-options.value.enabled", false) == true)
			.map(x -> ((JSONObject) x).getString("id")).collect(Collectors.toList());
		if(!handleRootAsSubInherit) {
			subInheritIds.remove(pageId);
		}
		log.debug("Found {} sub-inherits for {} and page {}", subInheritIds.size(), host.getBaseUrl(), pageId);
		
		for(String subInheritId : subInheritIds) {
			log.debug("Removing all descendants of sub-inherit {} for {} and page {}", subInheritId, host.getBaseUrl(), pageId);
			removeIntersect(allDescendants, getAllDescendants(host, useAccountId, subInheritId, true));
		}
		
		result.put("subInherits", subInheritIds);
		result.put("descendants", allDescendants);
		return result;
	}
	
	
	private JSONArray getAllDescendants(AtlassianHost host, String useAccountId, String pageId, boolean includeRoot) {
		JSONArray allDescendants = new JSONArray();
		try {
			int start = 0;
			
			if(includeRoot) {
				JSONObject root = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/api/v2/pages/%s?", host.getBaseUrl(), pageId));
				editInheritHelper.addJSONAtPath(root, editInheritHelper.getContentProperty(host, useAccountId, pageId, "edit-inherit-options"), "metadata.properties.edit-inherit-options");
				editInheritHelper.addJSONAtPath(root, editInheritHelper.getContentProperty(host, useAccountId, pageId, "edit-inherit-omission"), "metadata.properties.edit-inherit-omission");
				editInheritHelper.addJSONAtPath(root, editInheritHelper.getContentProperty(host, useAccountId, pageId, "edit-inherit-stamp"), "metadata.properties.edit-inherit-stamp");
				JSONObject permissions = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/content/%s/restriction/byOperation", host.getBaseUrl(), pageId));
				editInheritHelper.addJSONAtPath(root, permissions, "restrictions");
				allDescendants.put(root);
			}
			
			JSONObject batch = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/content/%s/descendant/page?limit=%d&"
				+ "expand=history,restrictions.update.restrictions.user,restrictions.update.restrictions.group,metadata.properties.edit-inherit-options,"
				+ "metadata.properties.edit-inherit-stamp,metadata.properties.edit-inherit-omission,restrictions.read.restrictions.user,restrictions.read.restrictions.group",
				host.getBaseUrl(), pageId, DESC_LIMIT));
			allDescendants.putAll(batch.getJSONArray("results"));
			while(batch.getInt("size") == DESC_LIMIT) {
				start += DESC_LIMIT;
				batch = editInheritHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/content/%s/descendant/page?start=%d&limit=%d&"
					+ "expand=history,restrictions.update.restrictions.user,restrictions.update.restrictions.group,metadata.properties.edit-inherit-options"
					+ ",metadata.properties.edit-inherit-stamp,metadata.properties.edit-inherit-omission,restrictions.read.restrictions.user,restrictions.read.restrictions.group",
					host.getBaseUrl(), pageId, start, DESC_LIMIT));
				allDescendants.putAll(batch.getJSONArray("results"));
				
				System.gc(); // suggest garbage collection
			}
		}
		catch(Exception e) {
			log.error("Error getting descendants for {} and page {}: {}", host.getBaseUrl(), pageId, e.toString());
			log.trace("Trace: ", e);
		}
		return allDescendants;
	}
	
	
	private JSONObject findDeviations(AtlassianHost host, String useAccountId, String pageId, String requestId) {
		JSONObject result = new JSONObject();
		List<String> nonCompliantDescendants = new ArrayList<String>();
		List<String> omittedDescendants = new ArrayList<String>();
		try {
			JSONObject allDescendantsResult = getRelevantDescendants(host, useAccountId, pageId, true, false);
			JSONArray allDescendants = allDescendantsResult.getJSONArray("descendants");
			String stamp = da.getString(allDescendants.getJSONObject(0), "metadata.properties.edit-inherit-stamp.value", "");
			for(int n = 1; n < allDescendants.length(); n++) {
				if(!da.getString(allDescendants.getJSONObject(n), "metadata.properties.edit-inherit-stamp.value", "").equals(stamp)) {
					nonCompliantDescendants.add(da.getString(allDescendants.getJSONObject(n), "id", ""));
				}
				if("true".equals(da.getString(allDescendants.getJSONObject(n), "metadata.properties.edit-inherit-omission.value", ""))) {
					omittedDescendants.add(da.getString(allDescendants.getJSONObject(n), "id", ""));
				}
			}
			
			result.put("nonCompliantDescendants", nonCompliantDescendants);
			result.put("subInherits", allDescendantsResult.getJSONArray("subInherits"));
			result.put("omittedDescendants", omittedDescendants);
		}
		catch(Exception e) {
			log.error("Error getting non compliant descendants for {} and page {}: {}", host.getBaseUrl(), pageId, e.toString());
			log.trace("Trace: ", e);
		}
		
		return result;
	}
	
	
	private void removeIntersect(JSONArray a1, JSONArray a2) {
		for(int n = 0; n < a2.length(); n++) {
			String id = a2.getJSONObject(n).getString("id");
			for(int m = 0; m < a1.length(); m++) {
				if(a1.getJSONObject(m).getString("id").equals(id)) {
					a1.remove(m);
					break;
				}
			}
		}
	}
	
	
	private List<String> splitNotEmpty(String stringToSplit) {
		if(stringToSplit.isEmpty()) {
			return new ArrayList<String>();
		}
		else {
			return new ArrayList<String>(Arrays.asList(stringToSplit.split(",")));
		}
	}
}
