package de.edrup.confluence.plugins.editinheritcloud.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ContextJwt;

import de.edrup.confluence.plugins.editinheritcloud.core.EditInheritCore;
import de.edrup.confluence.plugins.editinheritcloud.util.MiniJSONDotAccess;

@Controller
public class EditInheritController {
	
	@Autowired
	private EditInheritCore editInheritCore;
		
	private List<String> allowedRequests = Collections.synchronizedList(new ArrayList<String>());
	private static final Logger log = LoggerFactory.getLogger(EditInheritController.class);
	
	
	@RequestMapping(value = {"/dialog"}, method = RequestMethod.GET)
	public ModelAndView dialog(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="pageId") String pageId,
		@RequestParam(value="lic", defaultValue="") String lic) {
		
		ModelAndView model = new ModelAndView("edit-inherit-dialog");
		model.addObject("pageId", pageId);
		model.addObject("licensed", lic.equals("active") || hostUser.getHost().getBaseUrl().contains("edrup-dev"));
		model.addObject("baseUrl", hostUser.getHost().getBaseUrl());
		
		String requestKey = hostUser.getHost().getBaseUrl() + pageId + hostUser.getUserAccountId().get();
		if(!allowedRequests.contains(requestKey)) {
			allowedRequests.add(requestKey);
		}
		
		return model;
	}
	
	
	@RequestMapping(value = {"/inherit"}, method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> inherit(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestParam(value="pageId") String pageId) {
		
		String requestKey = hostUser.getHost().getBaseUrl() + pageId + hostUser.getUserAccountId().get();
		
		if(allowedRequests.contains(requestKey)) {
			allowedRequests.remove(requestKey);
			editInheritCore.inheritEditPermissions(hostUser.getHost(), hostUser.getUserAccountId().get(), pageId, pageId, false, false);
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<String>("", HttpStatus.METHOD_NOT_ALLOWED);
		}
	}
	
	
	@RequestMapping(value = {"/migrate"}, method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> migrate(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestParam(value="pageId") String pageId, @RequestParam(value="stampOnly") boolean stampOnly) {
		
		String requestKey = hostUser.getHost().getBaseUrl() + hostUser.getUserAccountId().get();
		
		if(allowedRequests.contains(requestKey)) {
			String useAccountId = editInheritCore.findSuitableAccountId(hostUser.getHost(), hostUser.getUserAccountId().get(), pageId, pageId);
			editInheritCore.inheritEditPermissions(hostUser.getHost(), useAccountId, pageId, pageId, false, stampOnly);
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<String>("", HttpStatus.METHOD_NOT_ALLOWED);
		}
	}
	
	
	@RequestMapping(value = {"/status"}, method = RequestMethod.GET)
	@ContextJwt
	public ResponseEntity<String> status(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestParam(value="pageId") String pageId) {
		
		return new ResponseEntity<String>(editInheritCore.getEditPermissionInheritanceStatus(hostUser.getHost(), hostUser.getUserAccountId().get(), pageId).toString(), noCache(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = {"/deviations"}, method = RequestMethod.GET)
	@ContextJwt
	public ResponseEntity<String> status(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="requestId", defaultValue="") String requestId, @RequestParam(value="pageId") String pageId) {
		
		return new ResponseEntity<String>(editInheritCore.getDeviations(hostUser.getHost(), hostUser.getUserAccountId().get(), pageId, requestId).toString(), noCache(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = {"/fix"}, method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> fixPage(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestParam(value="pageId") String pageId) {
		
		try {
			if(editInheritCore.hasEditPermissionInheritance(hostUser.getHost(), hostUser.getUserAccountId().get(), pageId)) {
				editInheritCore.inheritEditPermissions(hostUser.getHost(), hostUser.getUserAccountId().get(), pageId, pageId, true, false);
			}
		}
		catch(Exception e) {
			log.error("Could not fix for {} and page {}: {}", hostUser.getHost().getBaseUrl(), pageId, e.toString());
		}
		
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	

	@RequestMapping(value = {"/webhook/permission"}, method = RequestMethod.POST)
	public ResponseEntity<String> permissionUpdate(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		
		MiniJSONDotAccess da = new MiniJSONDotAccess();
		JSONObject webhook = new JSONObject(payload);
		
		try {
			log.debug("Webhook permission update received: {}", webhook.toString());
			
			String accountId = da.getString(webhook, "accountType", "").equals("app") ? "" : da.getString(webhook, "userAccountId", "");	
			// TODO: change to id (type String) after the transition period
			String pageId = da.getString(webhook, "content.idAsString", "0");
			if(da.getString(webhook, "content.contentType", "").equals("page") && !editInheritCore.shouldSkipEvent(pageId)) {
				if(editInheritCore.hasEditPermissionInheritance(hostUser.getHost(), accountId, pageId)) {
					editInheritCore.inheritEditPermissions(hostUser.getHost(), accountId,  pageId, pageId, false, false);
				}
			}
		}
		catch(Exception e) {
			log.error("Could not handle permission update webhook for {} with {}: {}", hostUser.getHost().getBaseUrl(), webhook.toString(), e.toString());
		}
		
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	
	@RequestMapping(value = {"/webhook/addedMoved"}, method = RequestMethod.POST)
	public ResponseEntity<String> addedMoved(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		
		MiniJSONDotAccess da = new MiniJSONDotAccess();
		JSONObject webhook = new JSONObject(payload);
		
		try {
			log.debug("Webhook added moved received: {}", webhook.toString());
			
			String accountId = da.getString(webhook, "accountType", "").equals("app") ? "" : da.getString(webhook, "userAccountId", "");	
			// TODO: change to id (type String) after the transition period
			String pageId = da.getString(webhook, "page.idAsString", "0");
			String inheritPageId = editInheritCore.getClosestEditPermissionInheritancePageId(hostUser.getHost(), accountId, pageId);
			if(inheritPageId != null) {
				log.debug("The page {} {} added by {} is covered by edit permission inheritance {}", hostUser.getHost().getBaseUrl(), pageId, accountId, inheritPageId);
				String useAccountId = editInheritCore.findSuitableAccountId(hostUser.getHost(), accountId, inheritPageId, pageId);
				if(useAccountId != null) {
					editInheritCore.inheritEditPermissions(hostUser.getHost(), useAccountId, inheritPageId, pageId, false, false);
				}
			}
		}
		catch(Exception e) {
			log.error("Could not handle added or moved webhook for {} with {}: {}", hostUser.getHost().getBaseUrl(), webhook.toString(), e.toString());
		}
		
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	
	@RequestMapping(value = {"/setInteritance"}, method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> setInheritance(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		
		String requestKey = hostUser.getHost().getBaseUrl() + hostUser.getUserAccountId().get();
		
		if(allowedRequests.contains(requestKey)) {
			JSONObject setInheritanceResult = editInheritCore.setInheritance(hostUser.getHost(), hostUser.getUserAccountId().get(), new JSONObject(payload));
			return new ResponseEntity<String>(setInheritanceResult.toString(), setInheritanceResult.getBoolean("success") ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
		}
		else {
			return new ResponseEntity<String>("", HttpStatus.METHOD_NOT_ALLOWED);
		}
	}
	
	
	@RequestMapping(value = "/migration", method = RequestMethod.GET)
	public ModelAndView migration(@AuthenticationPrincipal AtlassianHostUser hostUser) {
		
		String requestKey = hostUser.getHost().getBaseUrl() + hostUser.getUserAccountId().get();
		if(!allowedRequests.contains(requestKey)) allowedRequests.add(requestKey);
		
	    ModelAndView model = new ModelAndView("migration");
	    return model;
	}
	
	
	@RequestMapping(value = "/deviation", method = RequestMethod.GET)
	public ModelAndView deviation(@AuthenticationPrincipal AtlassianHostUser hostUser) {		
	    ModelAndView model = new ModelAndView("edit-inherit-deviation");
	    model.addObject("baseUrl", hostUser.getHost().getBaseUrl());
	    return model;
	}
	
	
	@RequestMapping(value = {"/descendants"}, method = RequestMethod.GET)
	@ContextJwt
	public ResponseEntity<String> getDescendants(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestParam(value="pageId") String pageId, @RequestParam(value="requestId") String requestId) {
		return new ResponseEntity<String>(editInheritCore.getDescendantsForReport(hostUser.getHost(), hostUser.getUserAccountId().get(), pageId, requestId).toString(), HttpStatus.OK);
	}
	
	
	private HttpHeaders noCache() {
		HttpHeaders headers = new HttpHeaders();
		headers.setCacheControl(CacheControl.noCache());
		return headers;
	}
}
