package de.edrup.confluence.plugins.editinheritcloud.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class EditInheritHelper {
	
	@Autowired
	private AtlassianHostRestClients atlassianHostRestClients;
	
	private static final int POOL_SIZE = 10;
	
	private static final Logger log = LoggerFactory.getLogger(EditInheritHelper.class);
	private static final MiniJSONDotAccess da = new MiniJSONDotAccess();


	public JSONObject getJSONObject(AtlassianHost host, String useAccountId, String url) {
		try {
			log.debug("Getting {}", url);
			return new JSONObject(getStringEntity(host, useAccountId, url).getBody());
		}
		catch(Exception e) {
			log.error("Could not get {}: {}", url, e.toString());
			return new JSONObject();			
		}
	}

	
	private ResponseEntity<String> getStringEntity(AtlassianHost host, String useAccountId, String url) {
		try {
			return authenticatedProvider(host, useAccountId).getForEntity(url, String.class);		
		}
		catch(Exception e) {
			log.error("Could not receive {} using accountId {}: {}", url, useAccountId, e.toString());
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	public boolean delete(AtlassianHost host, String useAccountId, String url) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<>(null, headers);
			authenticatedProvider(host, useAccountId).exchange(url, HttpMethod.DELETE, entity, String.class);
			return true;
		}
		catch(Exception e) {
			log.warn("Could not delete {}: {}", url, e.toString());
			return false;
		}
	}
	
	
	public int putNoPayload(AtlassianHost host, String useAccountId, String url) {
		try {
			authenticatedProvider(host, useAccountId).put(url, null);
			return 200;
		}
		catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
			if(!url.contains("accountId=1")) {
				log.error("Could not put {}: {}", url, httpClientOrServerExc.toString());
			}
			return httpClientOrServerExc.getStatusCode().value();
		}
	}
	
	
	public void setContentProperty(AtlassianHost host, String useAccountId, String containerId, String key, String value, JSONObject currentProperty) {
		setContentPropertyI(host, useAccountId, containerId, key, new JSONObject().put("value", value), currentProperty);
	}
	
	
	public void setContentProperty(AtlassianHost host, String useAccountId, String containerId, String key, JSONObject value, JSONObject currentProperty) {
		setContentPropertyI(host, useAccountId, containerId, key, new JSONObject().put("value", value), currentProperty);
	}
	
	
	private void setContentPropertyI(AtlassianHost host, String useAccountId, String containerId, String key, JSONObject payload, JSONObject currentProperty) {
		try {
			int version = da.getInt(currentProperty, "version.number", 0);

			payload.put("key", key);
			if(version > 0) payload.put("version", new JSONObject().put("number", version + 1));
			
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(payload.toString(), headers);

            if(version > 0) {
            	authenticatedProvider(host, useAccountId).exchange(String.format("%s/api/v2/pages/%s/properties/%s", host.getBaseUrl(), containerId, da.getString(currentProperty, "id", "")), HttpMethod.PUT, entity, String.class);
            }
            else {
            	authenticatedProvider(host, useAccountId).exchange(String.format("%s/api/v2/pages/%s/properties", host.getBaseUrl(), containerId), HttpMethod.POST, entity, String.class);            	
            }
		}
		catch(Exception e) {
			log.error("Could not set content property {} for {} and {} using accountId {}: {}", key, host.getBaseUrl(), containerId, useAccountId, e.toString());
		}
	}
	
		
	private RestTemplate authenticatedProvider(AtlassianHost host, String useAccountId) {
		if(useAccountId != null && !useAccountId.isEmpty()) {
			return atlassianHostRestClients.authenticatedAs(AtlassianHostUser.builder(host).withUserAccountId(useAccountId).build());
		}
		else {
			return atlassianHostRestClients.authenticatedAsAddon(host);
		}
	}
	
	
	public HashMap<String, JSONObject> getManyJSONObject(AtlassianHost host, String useAccountId, String urlWithPlaceHolder, List<String> elementIds) {
		HashMap<String, JSONObject> results = new HashMap<String, JSONObject>();
		ExecutorService pool = Executors.newFixedThreadPool(POOL_SIZE);
		
		try {
			RestTemplate runAs = authenticatedProvider(host, useAccountId);
			
			HashMap<String, CompletableFuture<JSONObject>> futureResults = new HashMap<String, CompletableFuture<JSONObject>>();
			for(String elementId : elementIds) {
				String url = urlWithPlaceHolder.replace("$", elementId);
				futureResults.put(elementId, CompletableFuture.supplyAsync(() -> new JSONObject(runAs.getForObject(url, String.class)), pool));
			}
	
			ArrayList<CompletableFuture<JSONObject>> allFutures = new ArrayList<CompletableFuture<JSONObject>>(futureResults.values());
			CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size()]));
			combinedFuture.get();
			
			for (String elementId : futureResults.keySet()) {
				results.put(elementId, futureResults.get(elementId).get()); 
			}
		}
		catch(Exception e) {
			log.error("Could not get many {}: {}", urlWithPlaceHolder, e.toString());
		}
		finally {
			pool.shutdown(); // Shut down the thread pool
			try {
				if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
					pool.shutdownNow(); // Cancel currently executing tasks
				} 
			} catch (InterruptedException ie) {
				pool.shutdownNow();
				Thread.currentThread().interrupt(); // Preserve interrupt status
			}
		}

		return results;
	}
	
	
	public JSONObject getContentProperty(AtlassianHost host, String useAccountId, String containerId, String key) {
		JSONArray results = getJSONObject(host, useAccountId, String.format("%s/api/v2/pages/%s/properties?key=%s", host.getBaseUrl(), containerId, key)).getJSONArray("results");
		return results.length() == 1 ? results.getJSONObject(0) : new JSONObject();
	}
	
	
	public void addJSONAtPath(JSONObject parent, JSONObject child, String path) {
        String[] keys = path.split("\\.");
        JSONObject current = parent;
        for (int i = 0; i < keys.length - 1; i++) {
            String key = keys[i];
            if (!current.has(key)) {
                current.put(key, new JSONObject());
            }
            current = current.getJSONObject(key);
        }
        current.put(keys[keys.length - 1], child);
    }
}
