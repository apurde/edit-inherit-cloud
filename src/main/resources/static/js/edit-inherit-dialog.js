/*jshint esversion: 11 */

function EditInheritDialog() {

	var pageId = "";
	var token = "";
	var baseUrl = "";
	var status = {};
	var nonCompliantList = [];

	this.init = function() {
		
		pageId = $("#edit-inherit-dialog-body").attr("data-page-id");
		token = $("meta[name=token]").attr("content");
		baseUrl =  $("#edit-inherit-dialog-body").attr("data-base-url");
		
		getStatus(function() {
			$("#load-spinner").hide();
			$("#can-inherit").toggle(status.canInherit);
			$("#cant-inherit").toggle(!status.canInherit);
			$("#message-running").toggle(status.running);
			$("#message-covered").toggle(status.closestPageId ? true : false);
			
			$("#edit-inherit-toggle").prop("checked", status.enabled);	
			
			$("#edit-inherit-options-select").val(status.options.split(",")).trigger("change");
			
			if(status.enabled && !status.running) {
				checkConsistency();
			}
		});
		
		$("#edit-inherit-options-select").auiSelect2({
			placeholder: $("#edit-inherit-options-select").data("placeholder")
		});
		
		$("#edit-inherit-options-select,#edit-inherit-toggle").change(function() {
			$("#edit-inherit-apply").attr("disabled", false);
			$("#edit-inherit-apply").attr("aria-disabled", false);
		});
		
		$("#edit-inherit-apply").click(function() {
			applyInheritance(function() {
				$("#edit-inherit-dialog-page").hide();
				$("#edit-inherit-dialog-reload").show();
				AP.navigator.reload();
			});
		});
		
		$("#edit-inherit-cancel").click(function() {
			AP.dialog.close({});
		});
		
		$("#edit-inherit-fix").click(function() {
			$("#message-consistency-nok").hide();
			$("#fixing-consistency").show();
			fixNonCompliant(function() {
				setTimeout(function() {
					waitUntilReady(function() {	
						$("#fixing-consistency").hide();
						checkConsistency();
					});
				}, 1000);
			});
		});
		
		$("#covered-link").click(function() {
			AP.navigator.go('contentview', {contentId: status.closestPageId});
		});
		
		$("#edit-inherit-deviation").click(function() {
			window.open(baseUrl + "/plugins/servlet/ac/de.edrup.confluence.plugins.edit-inherit/edit-inherit-deviation?pageId=" + pageId, '_blank').focus();	
		});
	};
	
	
	var checkConsistency = function() {
		$("#checking-consistency").show();
		getDeviations(Math.floor(Math.random() * 65000), function(result) {
			$("#checking-consistency").hide();
			if(result.nonCompliantDescendants.length > 0) {
				nonCompliantList = result.nonCompliantDescendants;
				$("#non-compliant").text(buildPageString(nonCompliantList, 5));
				$("#message-consistency-nok").show();
				console.log("Found the following non compliant descendants: " + nonCompliantList);
			}
			else {
				$("#sub-inherits").text(buildPageString(result.subInherits, 5));
				$("#omitted").text(buildPageString(result.omittedDescendants, 5));
				$("#message-consistency-ok").show();
				console.log("Found the following sub-inherits: " + result.subInherits);
				console.log("Found the following omissions due to missing view permissions: " + result.omittedDescendants);
			}
		});
	};
	
	
	var buildPageString = function(pageList, limit) {
		var pageString = pageList.length;
		if(pageList.length > 0) {
			pageString += " (" + (pageList.length > limit ? pageList.slice(0, limit).join(", ") + "..." : pageList.join(", ")) + ")";
		}
		return pageString;
	};
	
	
	var applyInheritance = function(callback) {
		var data = {
			enabled: $("#edit-inherit-toggle").prop("checked"),
			options: ($("#edit-inherit-options-select").val() || []).join()
		};
		restSaveProperty(pageId, "edit-inherit-options", data, function() {
			startInheritJob(function() {
				callback();
			});
		});
	};
	
	
	var restSaveProperty = function(containerId, propertyName, propertyValue, callback) {
		apiV2Helper.saveContentProperty({
			contentId: containerId,
			propertyKey: propertyName,
			propertyValue: propertyValue,
			success: callback,
			error: function() {
				showErrorFlag($("#errorSaveBody").text());
			}
		});
	};
	
	
	var startInheritJob = function(callback) {
		$.ajax({
			url: "/inherit?pageId=" + pageId,
			type: "POST",
			beforeSend: function (request) {
			    request.setRequestHeader("Authorization", "JWT " + token);
			},
			success: function(result) {
				callback();
			},
			error: function() {
				showErrorFlag($("#errorStartBody").text());
			}
		});	
	};
	
	
	var getDeviations = function(requestId, callback) {
		$.ajax({
			url: "/deviations?requestId=" + requestId + "&pageId=" + pageId,
			beforeSend: function (request) {
			    request.setRequestHeader("Authorization", "JWT " + token);
			},
			success: function(resultText) {
				var result = JSON.parse(resultText);
				if(result.finished) {
					callback(result);
				}
				else {
					setTimeout(function() {
						getDeviations(requestId, callback);
					}, 1000);
				}
			},
			error: function() {
				showErrorFlag($("#errorGetBody").text());
			}
		});	
	};
	
	
	var fixNonCompliant = function(callback) {
		$.ajax({
			url: "/fix?pageId=" + pageId,
			type: "POST",
			beforeSend: function (request) {
			    request.setRequestHeader("Authorization", "JWT " + token);
			},
			success: function(result) {
				callback();
			},
			error: function() {
				showErrorFlag($("#errorStartBody").text());
			}
		});	
	};
	
	
	var getStatus = function(callback) {
		$.ajax({
			url: "/status?pageId=" + pageId,
			beforeSend: function (request) {
			    request.setRequestHeader("Authorization", "JWT " + token);
			},
			success: function(resultText) {
				status = JSON.parse(resultText);
				callback();
			},
			error: function() {
				showErrorFlag($("#errorGetBody").text());
			}
		});	
	};
	
	
	var waitUntilReady = function(callback) {
		getStatus(function() {
			if(status.running) {
				setTimeout(function() {
					waitUntilReady(callback);
				}, 1000);
			}
			else {
				callback();
			}
		});
	};
	
	
	var showErrorFlag = function(bodyText) {
		AP.flag.create({
			title: $("#errorTitle").text(),
			body: bodyText,
			type: "error",
			close: "auto"
		});
	};
}

var editInheritDialog = new EditInheritDialog();
editInheritDialog.init();