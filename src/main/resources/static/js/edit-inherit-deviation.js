/*jshint esversion: 11 */

function EditInheritDeviation() {
	
	var baseUrl;
	
	
	this.run = async function() {
		baseUrl = $("#deviation-container").attr("data-base-url");
		AP.navigator.getLocation(async function(result) {
			const callingUrl = new URL(result.href);
			const urlParams = new URLSearchParams(callingUrl.search);
			const pageId = urlParams.get('pageId');
			if(pageId) {
				var descendantData = await getDescendantsAsync(pageId, crypto.randomUUID());
				if (descendantData) {
			        const evaluatedData = evaluateDescendants(descendantData);
			        appendToResult(pageId, evaluatedData);
			        bindActions(pageId);
			    }
				$("#load-spinner").hide();
			}
			else {
				var pages = await getAllEPIs();
				var tasks = pages.map(async (page) => {
				    var descendantData = await getDescendantsAsync(page.content.id, crypto.randomUUID());
				    if (descendantData) {
				        const evaluatedData = evaluateDescendants(descendantData);
				        appendToResult(page.content.id, evaluatedData);
				        bindActions(page.content.id);
				    }
				});
				
				await Promise.all(tasks);
				
				$("#load-spinner").hide();
			}
		});
	};
	
	
	var getAllEPIs = async function() {
		var pages = [];

		var url = '/rest/api/search?limit=200&cql=editinheritenabled="true"';
		var hasNext = true;
		
		while(hasNext) {
			var searchRequest = await AP.request(url);
			var searchResults = JSON.parse(searchRequest.body);
			pages = pages.concat(searchResults.results);
			url = searchResults._links.next || "";
			hasNext = url.length > 0;
		}
		
		return pages;
	};
	
	
	var getDescendantsAsync = async function(pageId, requestId) {
		return new Promise((resolve) => {
			getDescendants(pageId, requestId, function(result) {
				return resolve(result);
			});
		});
	};
	
	
	var getDescendants = function(pageId, requestId, callback) {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/descendants?pageId=" + pageId + "&requestId=" + requestId,
				beforeSend: function (request) {
				    request.setRequestHeader("Authorization", "JWT " + token);
				},
				success: function(resultText) {
					var result = JSON.parse(resultText);
					if(result.finished) {
						callback(result);
					}
					else {
						setTimeout(function() {
							getDescendants(pageId, requestId, callback);
						}, 1000);
					}
				},
				error: function() {
					showErrorFlag($("#errorGetDescendants").text());
					callback(null);
				}
			});	
		});
	};
	

	var evaluateDescendants = function(descendantData) {
		
		var parentPermissions = getEditPermissions(descendantData.descendants[0].restrictions.update);
		var deviations = {
			"parent" : {
				"title": descendantData.descendants[0].title,
				"link": baseUrl + descendantData.descendants[0]._links.webui,
				"editPermissions": parentPermissions,
			},
			descendants: []
		};
		
		for(var n = 1; n < descendantData.descendants.length; n++) {
			var childPermissions = getEditPermissions(descendantData.descendants[n].restrictions.update);
			if(hasDeviations(parentPermissions, childPermissions)) {
				deviations.descendants.push({
					"title": descendantData.descendants[n].title,
					"id": descendantData.descendants[n].id,
					"link": baseUrl + descendantData.descendants[n]._links.webui,
					"editPermissions": addDeviationDetails(parentPermissions, getEditPermissions(descendantData.descendants[n].restrictions.update))
				});
			}
		}
		
		return deviations;
	};
	
	
	var appendToResult = function(pageId, deviationResult) {
		// Start with the parent container
		var parentHtml = $('<div class="parent"></div>');

		// Add parent title
		var parentTitle = $('<div class="parent-title"></div>');
		parentTitle.append($('<a></a>').attr('href', deviationResult.parent.link).attr('target', "_blank").text(deviationResult.parent.title));
		parentHtml.append(parentTitle);

		// Loop through parent editPermissions
		deviationResult.parent.editPermissions.forEach(function(permission) {
			parentHtml.append($('<div class="permission"></div>').text(permission.name));
		});

		// Container for descendants
		var descendantsContainer = $('<div></div>');

		// Loop through each descendant
		deviationResult.descendants.forEach(function(descendant) {
			var descendantDiv = $('<div class="descendant"></div>');
			var descendantTitle = $('<div class="descendant-title"></div>');
			descendantTitle.append($('<a></a>').attr('href', descendant.link).attr('target', "_blank").text(descendant.title));
			descendantDiv.append(descendantTitle);

			// Loop through editPermissions of each descendant
			descendant.editPermissions.forEach(function(permission) {
				if (permission.isAdditional) {
					var additionalPermissionDiv = $('<div class="permission additional-permission"></div>');
					additionalPermissionDiv.append($('<span></span>').text(permission.name));
					additionalPermissionDiv.append($('<span class="permission-remove aui-icon aui-icon-small aui-iconfont-trash"></span>')
						.attr('data-permission-type', permission.type)
						.attr('data-permission-id', permission.id)
						.attr('data-page-id', descendant.id));
					descendantDiv.append(additionalPermissionDiv);
				} else {
					descendantDiv.append($('<div class="permission"></div>').text(permission.name));
				}
			});

			descendantsContainer.append(descendantDiv);
		});

		// Append everything to the main container
		var mainContainer = $('<div class="deviation-result-container"></div>').attr('id', "deviations-of-" + pageId).append(parentHtml).append(descendantsContainer);

		// Assuming you want to append this to an element with id="results"
		$('#results').append(mainContainer);
	};
	
	
	var bindActions = function(pageId) {
		$("#deviations-of-" + pageId).find(".permission-remove").click(function() {
			const elem = this;
			const url = "/rest/api/content/" + $(elem).attr("data-page-id") + "/restriction/byOperation/update/" +
				($(elem).attr("data-permission-type") === "user" ? "user?accountId=" : "byGroupId/") +
				$(elem).attr("data-permission-id");
				
			AP.request({
				type: "DELETE",
				url: url,
				success: function() {
					$(elem).closest(".permission").remove();
				},
				error: function() {
					showErrorFlag($("#errorDelete").text());		
				}
			});
		});
	};
	
	
	var getEditPermissions = function(updateNode) {
		var results = updateNode.restrictions.user.results.map((restriction) => {
			return {
				"id" : restriction.accountId,
				"name": restriction.displayName,
				"type": "user"
			};
		});
		
		results.push(...updateNode.restrictions.group.results.map((restriction) => {
			return {
				"id" : restriction.id,
				"name": restriction.name,
				"type" : "group"
			};
		}));
		
		return results;
	};
	
	
	var hasDeviations = function(parentPermissions, childPermissions) {
		return parentPermissions.length !== childPermissions.length ||
			!parentPermissions.every((parentPermission) => childPermissions.some((childPermission) => childPermission.id === parentPermission.id));
	};
	
	
	var addDeviationDetails = function(parentPermissions, childPermissions) {
		return childPermissions.map((childPermission) => {
			return {
				"id": childPermission.id,
				"name": childPermission.name,
				"type": childPermission.type,
				"isAdditional": !parentPermissions.some((parentPermission) => parentPermission.id === childPermission.id)
			};
		});
	};
	
	
	var showErrorFlag = function(bodyText) {
		AP.flag.create({
			title: $("#errorTitle").text(),
			body: bodyText,
			type: "error"
		});
	};
}

var editInheritDeviation = new EditInheritDeviation();
editInheritDeviation.run();