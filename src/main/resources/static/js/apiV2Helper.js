/*jshint esversion: 11 */

function APIV2Helper() {
	
	this.getContentProperty = async function(params) {
		try {
			var contentType = await getContentTypeFromId(params.contentId);
			var propertyResult = await getContentPropertyWithKey(params.contentId, contentType, params.propertyKey);
			if(propertyResult.results.length > 0) {
				if(params.success) params.success(propertyResult.results[0]);
				else return propertyResult.results[0];
			}
			else
			{
				if(params.error) params.error("Property not found");
				else throw new Error("Property not found");
			}
			
		}
		catch(error) {
			if(params.error) params.error(error);
			else throw error;
		}
	};
	
	
	this.saveContentProperty = async function(params) {
		try {
			var contentType = await getContentTypeFromId(params.contentId);
			var propertyResult = await getContentPropertyWithKey(params.contentId, contentType, params.propertyKey);
			
			if(propertyResult.results.length > 0) {
				const data = {
					version: {
						number: propertyResult.results[0].version.number + 1
					},
					key: params.propertyKey,
					value: params.propertyValue
				};
				AP.request({
					type: "PUT",
					url: "/api/v2/" + contentType + "s/" + params.contentId + "/properties/" + propertyResult.results[0].id,
					data: JSON.stringify(data),
					contentType : "application/json",
					dataType : "json",
					success: function(result) {
						if(params.success) params.success(result);
						else return result;
					},
					error: function() {
						if(params.error) params.error();
						else throw new Error("Property can't be saved");
					}
				});
			}
			else {
				const data = {
					key: params.propertyKey,
					value: params.propertyValue
				};
				AP.request({
					type: "POST",
					url: "/api/v2/" + contentType + "s/" + params.contentId + "/properties",
					data: JSON.stringify(data),
					contentType : "application/json",
					dataType : "json",
					success: function(result) {
						if(params.success) params.success(result);
						else return result;
					},
					error: function() {
						if(params.error) params.error();
						else throw new Error("Property can't be saved");
					}
				});
			}
		}
		catch(error) {
			if(params.error) params.error(error);
			else throw error;
		}
	};
	
	
	this.getPagesByTitleAndSpaceKey = async function(params) {
		try {
			const spaceRequest = await AP.request("/api/v2/spaces?keys=" + params.spaceKey);
			const spaceResult = JSON.parse(spaceRequest.body);
			const pageRequest = await AP.request("/api/v2/pages?title=" + encodeURIComponent(params.pageTitle) + "&space-id=" + spaceResult.results[0].id);
			if(params.success) params.success(JSON.parse(pageRequest.body));
			else return JSON.parse(pageRequest.body);
			
		}
		catch(error) {
			if(params.error) params.error(error);
			else throw error;
		}
	};
	
	
	
	var getContentTypeFromId = async function(contentId) {
		try {
			var typeRequest = await AP.request({
				"url": "/api/v2/content/convert-ids-to-types",
				"type": "POST",
				"contentType": "application/json; charset=utf-8",
				"dataType" : "json",
				"data" : JSON.stringify({
					"contentIds" : [contentId]
				})
			});
			return JSON.parse(typeRequest.body).results[contentId];
		}
		catch(error) {
    		throw error;
		}
	};
	
	
	var getContentPropertyWithKey = async function(contentId, contentType, propertyKey) {
		try {
			var propertyRequest = await AP.request("/api/v2/" + contentType + "s/" + contentId + "/properties?key=" + propertyKey);
			return JSON.parse(propertyRequest.body);
		}
		catch(error) {
    		throw error;
		}
	};
}

var apiV2Helper = new APIV2Helper();