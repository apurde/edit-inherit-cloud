/*jshint esversion: 9 */

function EditInheritMigration() {
	
	this.init = function() {
		
		$("#simulate").click(function() {
			$("#results").empty();
			doMigrate(true);
		});
		
	
		$("#migrate").click(function() {
			$("#results").empty();
			doMigrate( false);
		});		
	};
	
	
	var doMigrate = async function(simulate) {
		try {
			$("#migration-spinner").show();
			$("#simulate,#migrate").hide();
	
			logInfo("Starting migration");
			
			var source = JSON.parse($("#source").val());
			var stampOnly = $("#stamp-only").prop('checked');
			
			for(var n = 0; n < source.length; n++) {
				await migrateInheritance(source[n], simulate, 1, stampOnly);
			}
			
			for(var n = 0; n < source.length; n++) {
				await migrateInheritance(source[n], simulate, 2, stampOnly);
			}

		}
		catch(error) {
			logError(error.message);	
		}
		
		logInfo("Migration completed");
		$("#migration-spinner").hide();
		$("#simulate,#migrate").show();
	};
	
	
	var migrateInheritance = async function(inheritance, simulate, step, stampOnly) {
		return new Promise(async (resolve, reject) => {
			var result;
			var waiting = false;
			try {
				if(inheritance.active) {
					var pageId = await findPageId(inheritance.title, inheritance.spaceKey);
					if(pageId) {
						if(step === 1) {
							var options = (inheritance.mode & 2) > 0 ? "hard," : "";
							options +=  (inheritance.mode & 8) > 0 ? "creator," : "";
							options +=  (inheritance.mode & 16) > 0 ? "delta," : "";
							options = options.replace(/,$/, ''); 
							logInfo("Setting edit permission inheritance on page " + pageId + " with options " + options);
							if(!simulate) {
								result = await setInheritance(pageId, options);
								if(!result.success) {
									logError("Error setting edit permission inhertiance on page " + pageId +": " + result.error);	
								}
							}
						}
						else {
							logInfo("Performing inheritance on page " + pageId + " with quick mode " + stampOnly);
							if(!simulate) {
								result = await doInherit(pageId, stampOnly);
								if(!result.success) {
									logError("Error starting edit permission inhertiance on page " + pageId +": " + result.error);
								}
								else {
									waiting = true;
									var checkInterval = setInterval(function() {
										checkStatus(pageId, function(response) {
											if(!response.status.running) {
												clearInterval(checkInterval);
												waiting = false;
												resolve();
											}
										});
									}, 1000);
								}
							}
						}
					}
				}
			}
			catch(error) {
				logError("Error migrating inheritance " + JSON.stringify(inheritance) + ": " + error.message);
			}
			
			if(!waiting) resolve();
		});
	};
	
	
	var findPageId = async function(pageTitle, spaceKey) {
		try {
			var searchResults = await apiV2Helper.getPagesByTitleAndSpaceKey({
				pageTitle: pageTitle,
				spaceKey: spaceKey
			});			
			if(searchResults.results.length === 1) {
				return searchResults.results[0].id;
			}
			else {
				logError("Error finding page with title " + pageTitle + " in space with key " +  spaceKey);	
				return null;
			}
		}
		catch(error) {
			logError("Error finding page with title " + pageTitle + " in space with key " +  spaceKey);	
			return null;
		}
	};
	
	
	var setInheritance = async function(pageId, options) {
		return new Promise((resolve, reject) => {
			var bodyData = {
				pageId : pageId,
				options : options
			};
			AP.context.getToken(function(token){
				$.ajax({
					url: "/setInteritance",
					type: "POST",
					data: JSON.stringify(bodyData),
					contentType : "application/json",
					beforeSend: function (request) {
						request.setRequestHeader("Authorization", "JWT " + token);
					},
					success: function() {
						return resolve({success : true});
					},
					error: function(request, status, error) {
						return resolve({
							success : false,
							status : request.status,
							error: buildErrorMessage(request, status, error)
						});
					}
				});
			});	
		});		
	};
	
	
	var doInherit = async function(pageId, stampOnly) {
		return new Promise((resolve, reject) => {
			AP.context.getToken(function(token){
				$.ajax({
					url: "/migrate?pageId=" + pageId + "&stampOnly=" + stampOnly,
					type: "POST",
					contentType : "application/json",
					beforeSend: function (request) {
						request.setRequestHeader("Authorization", "JWT " + token);
					},
					success: function() {
						return resolve({success : true});
					},
					error: function(request, status, error) {
						return resolve({
							success : false,
							status : request.status,
							error: buildErrorMessage(request, status, error)
						});
					}
				});
			});	
		});		
	};
	
	
	var checkStatus = function(pageId, callback) {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/status?pageId=" + pageId,
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", "JWT " + token);
				},
				success: function(response) {
					callback({success : true, status : JSON.parse(response)});
				},
				error: function(request, status, error) {
					callback({
						success : false,
						status : request.status,
						error: buildErrorMessage(request, status, error)
					});
				}
			});
		});	
	};
		
	
	var sanitizeText = function(text) {
		return $('<div/>').text(text).html();
	};
	
	
	var logError = function(message) {
		console.error(message);
		addToResults(message, "red");
	};
	
	
	var logInfo = function(message) {
		addToResults(message, "inherit");
	};
	
	
	var addToResults = function(message, color) {
		// security hint: color is only defined within this JS file, the message could contain malicious code and is therefore escaped
		$("#results").append('<p style="color:' + color + '">' + sanitizeText(message) + '</p>');
	};
	
	
	var buildErrorMessage = function(request, status, error) {
		var message = request.status + ";" + status + ";" + error;
		try {
			var response = JSON.parse(request.responseText);
			message = response.error || message;	
		}
		catch(error) {}
		return message;
	};

}

var editInheritMigration = new EditInheritMigration();
editInheritMigration.init();
